protocol Carable : Hashable
{
    var autoMark : AutoMark {get};
    var year : Int {get}; // год выпуска
    var volume : Double {get}; // объем багажника/кузова
    var isActive : Bool {get}; // запущен ли двигатель
    var isOpenWindow : Bool {get}; // открыты ли окна
    var storeVolume : Double {get}; // заполненный объем багажника
    // Добавить в структуры метод с одним аргументом типа перечисления, который будет менять свойства структуры в зависимости от действия
    mutating func action(action : AutoAction);
}

enum AutoMark
{
    case Volvo
    case MAN 
    case BMV
}
enum AutoAction // Описать перечисление с возможными действиями с автомобилем
{
    case Start // запустить двигатель
    case Stop // заглушить двигатель
    case OpenWindow // открыть окна
    case ClouseWindow // закрыть окна
    case Store(volume : Double) // погрузить в кузов/багажник груз определенного объема
    case UnStore(volume : Double) // выгрузить из кузова/багажника груз определенного объема
}
// Описать несколько структур
struct StorableAuto : Carable // любой грузовик
{
    let autoMark : AutoMark;
    let year : Int;
    let volume : Double;
    var isActive : Bool;
    var isOpenWindow : Bool;
    var storeVolume : Double;
    mutating func action(action : AutoAction)
    {
        switch action
        {
            case .Start :  do
            {
                if isActive
                {
                    print("Вы пытаетесь повернуть ключ, но ничего не происходит");
                }
                else
                {
                    isActive = true;
                    print("Машина завелась");
                }
            }
            case .Stop :  do
            {
                isActive = false;
                print("Двигатель не издает никаких звуков")
            }
            case .OpenWindow :  do
            {
                isOpenWindow = true;
            }
            case .ClouseWindow :  do
            {
                isOpenWindow = false;
            }
            case .Store(let volume) :  do
            {
                if volume + storeVolume > volume
                {
                    print("Все и так уже забито");
                }
                else
                {
                    storeVolume += volume;
                }
            }
            case .UnStore(let volume) :  do
            {
                if volume > storeVolume
                {
                    print("У ас нет столько товаров!");
                }
                else
                {
                    storeVolume -= volume;
                }
            }
        }
    }
}

struct SimpleAuto : Carable // любой легковой автомобиль
{
    let autoMark : AutoMark;
    let year : Int;
    let volume : Double;
    var isActive : Bool;
    var isOpenWindow : Bool;
    var storeVolume : Double;
    mutating func action(action : AutoAction)
    {
        switch action
        {
            case .Start :  do
            {
                if isActive
                {
                    print("Вы пытаетесь дергать за ключ, но машина наоборот глохнет");
                    isActive = true;
                }
                else
                {
                    isActive = true;
                    print("Машина завелась");
                }
            }
            case .Stop :  do
            {
                isActive = false;
                print("Двигатель не издает никаких звуков")
            }
            case .OpenWindow :  do
            {
                isOpenWindow = true;
            }
            case .ClouseWindow :  do
            {
                isOpenWindow = false;
            }
            case .Store(let volume) :  do
            {
                if volume + storeVolume > volume * 1.2
                {
                    print("Все и так уже забито");
                }
                else
                {
                    storeVolume += volume;
                }
            }
            case .UnStore(let volume) :  do
            {
                if volume > storeVolume
                {
                    print("У ас нет столько товаров!");
                }
                else
                {
                    storeVolume -= volume;
                }
            }
        }
    }
}

// Инициализировать несколько экземпляров структур. Применить к ним различные действия.
var simpleAuto1 : SimpleAuto = SimpleAuto(autoMark : AutoMark.BMV, year : 2001, volume : 20, isActive : false, isOpenWindow : true, storeVolume : 5);
var storableAuto : StorableAuto = StorableAuto(autoMark : AutoMark.MAN, year : 1997, volume : 5000, isActive : false, isOpenWindow : false, storeVolume : 3000);
var simpleAuto2 : SimpleAuto = SimpleAuto(autoMark : AutoMark.Volvo, year : 2027, volume : 3, isActive : true, isOpenWindow : false, storeVolume : 0);
simpleAuto1.action(action : AutoAction.Start);
simpleAuto1.action(action : AutoAction.Stop);
// Положить объекты структур в словарь как ключи, а их названия как строки
var dictionary : [AnyHashable : String] = [simpleAuto1: "БМВ", storableAuto: "MAN", simpleAuto2: "Вольво"];

// Capture List
var clouser3 : () -> () = {};
if true
{
    var x : Int = 10;
    let clouser1 : () -> () = { print(x); } // Пусть у нас есть клоужерс, который вовдит значение (внешней для него) переменной x
    clouser1(); // Здесь очевидно выведется 10
    x = 5;
    clouser1(); // А Здесь будет 5, потому что clouser1 ссылается на переменую x, а не вытаскивает значение из переменной
    // Если мы хотим испоьлзовать только значение действительное на момент создания клоужерса необходимо испольльзовать Capture List
    // перед самим действием используем "[varName1, varName2...] in" тогда значения перечисленных переменых будут зафиксированы
    let clouser2 : () -> () = { [x] in print(x);}
    clouser2(); // Здесь очевидно 5
    x = 15;
    clouser2(); // И здесь 5 тоже
    // Важно, что захват переменной так же является ссылкой на нее, поэтому захваченая переменая может существоать дольше области своей видимости
    clouser3 = { print(x);}
    clouser3(); // Здесь выведется 15
    x = 20;
}
clouser3(); // Здесь выведется 20
// поэтому для объектов классов в CaptureList можно указывать weak и unowned

class Car
{
    weak var driver : Man?; // Используем слабую ссылку
    deinit
    {
        print("Машина удалена из памяти"); 
    }
}

class Man
{
    var myCar : Car?;
    deinit
    {
        print("Человек удален из памяти"); 
    }
}
var car : Car? = Car();
var man : Man? = Man();
car?.driver = man;
man?.myCar = car;
car = nil; // man все ещё ссылается
man = nil; // сильных ссылок больше  нет, удаляем
// на car тоже больше нет ссылок удаляем

class ManWithPassport
{
    var passport : Passport?; // Здесь weak и unowned использовать нельзя
    deinit
    {
        print("Человек удален из памяти"); 
    }
}

class Passport
{
    unowned let man : ManWithPassport // Применим unowned здесь 
    init(man : ManWithPassport)
    {
        self.man = man;
    }
    deinit
    {
        print("Паспорт удален из памяти"); 
    }
}

var manWithPassport : ManWithPassport? = ManWithPassport();
var passport : Passport? = Passport(man : manWithPassport!);
manWithPassport?.passport = passport;
passport = nil;
manWithPassport = nil;